################################################################################
#
# App: example_4_middleware
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_4_middleware
#     julia main_middleware.jl
# - In your browser go to localhost:8000/home
#
################################################################################


################################################################################
### Includes
################################################################################
using HttpCommon    # Defines FileResponse (among other things)
using HttpServer    # Basic http/websockets server
using Meddle        # Contains middleware: a sequence functions that each modify the request and response. 
using Mustache      # For populating html templates with requested data
using DataFrames    # For storing data in memory
using JSON          # For converting julia dictionaries to JSON data
using GLM           # For regression models

include("utils.jl")       # Utility functions
include("handlers.jl")    # Contains our handler functions
include("midware.jl")     # Contains the functions that operate on each request/response pair


################################################################################
### Utility variables
################################################################################
const routes = Dict{String, Function}()                 # Map each route to a handler: route => handler
const df     = readtable("../../data/iris_data.csv")    # Raw data from csv, database, website, etc


################################################################################
### Application logic
################################################################################

# Associate each route with a handler
routes["/home"]       = home
routes["/model"]      = iris_model
routes["/2charts"]    = iris_2charts
routes["/table"]      = show_table
routes["/map_chloro"] = map_chloropleth


# A middleware "stack" is a sequence of functions that operate on each request/response pair
# Meddle.handle loops through each function in the stack.
# At each iteration, the function either:
# - Completes the request (res.finished == true), breaks the loop and returns the response, OR
# - Modifies res and passes (req, res) to the next function in the stack
stack = middleware(ServeStaticFiles, HandleValidRequests, BadRequest)


# Dispatch request to appropriate handler
function mainhandler(req, res)
    Meddle.handle(stack, Meddle.MeddleRequest(req, Dict(), Dict()), res)
end


# Instantiate and run server
server = Server(mainhandler)    # Create Server instance with server.http.handle = mainhandler
run(server, 8000)               # This is a blocking function
