using SHA      # For hashing a randomly generated session ID
using Dates    # Not required for Julia version 4


function generate_session_id()
    # Generate random session id
    sha256(string(rand()))
end


function set_cookie(session_id)
    # Returns: the response cookie value
    #
    # The Max-Age attribute is not sent to the server on subsequent requests.
    # Instead, it tells the client browser to delete the cookie after 1800 seconds = 30 mins.
    "sessionid=$session_id; Max-Age=1800"
end
