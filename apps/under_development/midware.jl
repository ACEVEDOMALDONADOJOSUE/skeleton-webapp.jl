################################################################################
### Functionality
################################################################################


function create_session(req, res)
    # Create a new session ID and record basic session information
    if !haskey(req.http_req.headers, "Cookie")
        session_id = generate_session_id()
        res.headers["Set-Cookie"] = set_cookie(session_id)
        sessions[session_id] = ["some_key" => "some_value"]
    end
    req, res
end


function serve_static(req, res)
    # Serve files from static/ folder.
    # This function uses HttpCommon.FileResponse rather than Meddle.FileServer because the latter:
    # - Permits access to any file on the filesystem
    # - Ignores mime types  
    if ismatch(r"/static/*", req.http_req.resource)
        res2 = serve_static_files(req, res)
        res.status  = res2.status
        res.headers = res2.headers
        res.data    = res2.data
        return respond(req, res)
    end
    req, res
end


function handle_valid_requests(req, res)
    if haskey(routes, req.http_req.resource)
        routes[req.http_req.resource](req, res)
        return respond(req, res)
    end
    req, res
end


function bad_request(req, res)
    res.status = 400
    res.data   = "<h1>Bad request:</h1> <h3>$(req.http_req.resource[2:end])</h3> <h3>is not a valid resource.</h3>"
    respond(req, res)
end


################################################################################
### Map names of each midware object to its functionality
################################################################################

CreateSession       = Midware(create_session)
ServeStaticFiles    = Midware(serve_static)
HandleValidRequests = Midware(handle_valid_requests)
BadRequest          = Midware(bad_request)
