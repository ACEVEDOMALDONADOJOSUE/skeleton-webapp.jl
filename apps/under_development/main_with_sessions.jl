################################################################################
### Includes
################################################################################
using HttpCommon    # Defines several http-related objects such as Request, Response and FileResponse() (which serves static files)
using HttpServer    # Basic http/websockets server
using Meddle        # Contains middleware: a sequence functions that each modify the request and response. 
using Mustache      # For populating html templates with requested data
using DataFrames    # For storing data in memory
using GLM           # For regression models
using JSON          # For converting julia dictionaries to JSON data

include("handlers.jl")    # Contains our handler functions
include("midware.jl")     # Contains the functions that operate on each request/response pair
include("utils.jl")       # Utility functions


################################################################################
### Utility variables
################################################################################
const routes   = Dict{String, Function}()     # Map each route to a handler: route => handler
const sessions = Dict{ASCIIString, Dict}()    # Store session information:   session_id => Dict
const df = readtable("data/iris_data.csv")    # Raw data from csv, database, website, etc


################################################################################
### Application logic
################################################################################

# Associate each route with a handler
# Routes that are filenames are handled separately
routes["/home"]       = home
routes["/model"]      = iris_model
routes["/2charts"]    = iris_2charts
routes["/map_chloro"] = map_chloropleth

# The sequence of functions that operate on each request/response pair
stack = middleware(CreateSession, ServeStaticFiles, HandleValidRequests, BadRequest)

# Instantiate and run server
function mainhandler(req, res)
    # Meddle.handle loops through each function in the stack.
    # At each iteration, the function either:
    # - Completes the request (res.finished == true) and breaks the loop.
    # - Modifies res and passes (req, res) to the next function in the stack
    Meddle.handle(stack, Meddle.MeddleRequest(req, Dict(), Dict()), res)
end
server = Server(mainhandler)    # Create Server instance with server.http.handle = mainhandler
run(server, 8000)               # This is a blocking function
