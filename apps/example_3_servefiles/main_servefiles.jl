################################################################################
#
# App: example_3_servefiles
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_3_servefiles
#     julia main_servefiles.jl
# - In your browser go to localhost:8000/home
#
################################################################################


################################################################################
### Includes
################################################################################
using HttpCommon    # Defines FileResponse (among other things)
using HttpServer    # Basic http/websockets server
using Mustache      # For populating html templates with data
using DataFrames    # For storing data in memory
using JSON          # For converting julia dictionaries to JSON data
using GLM           # For regression models

include("utils.jl")       # Utility functions
include("handlers.jl")    # Contains our handler functions


################################################################################
### Utility variables
################################################################################
const routes = Dict{String, Function}()                 # Map each route to a handler: route => handler
const df     = readtable("../../data/iris_data.csv")    # Raw data from csv, database, website, etc


################################################################################
### Application logic
################################################################################

# Associate each route with a handler
routes["/home"]       = home
routes["/model"]      = iris_model
routes["/2charts"]    = iris_2charts
routes["/table"]      = show_table
routes["/map_chloro"] = map_chloropleth


# Dispatch request to appropriate handler
function mainhandler(req::Request, res::Response)
    if ismatch(r"^/static/*", req.resource)    # Serve static file if requested
        serve_file(req, res)
    elseif haskey(routes, req.resource)        # else consult routes table for handler
        routes[req.resource](req, res)
    else                                       # else requested resource not found
        res.status = 404
        res.data   = "Requested resource not found"
    end
    res
end


# Instantiate and run server
server = Server(mainhandler)    # Create Server instance with server.http.handle = mainhandler
run(server, 8000)               # This is a blocking function
