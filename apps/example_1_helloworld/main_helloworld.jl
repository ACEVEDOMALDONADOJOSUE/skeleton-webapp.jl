################################################################################
#
# App: example_1_helloworld
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_1_helloworld
#     julia main_helloworld.jl
# - In your browser go to localhost:8000/home
#
################################################################################


# Include dependencies
# - HttpServer uses HttpCommon, which defines several http-related objects such as Request and Response
using HttpServer    # Basic http/websockets server


# Define request handling
function mainhandler(req, res)
    if req.resource == "/home"
        res.data = "Hello world!"
    else
        res.status = 404
        res.data   = "Requested resource not found"
    end
    res
end


# Instantiate and run server
server = Server(mainhandler)    # Create Server instance with server.http.handle = mainhandler
run(server, 8000)               # This is a blocking function
