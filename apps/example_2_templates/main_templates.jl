################################################################################
#
# App: example_2_templates
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_2_templates
#     julia main_templates.jl
# - In your browser go to localhost:8000/home
#
################################################################################


# Include dependencies
# - HttpServer uses HttpCommon, which defines several http-related objects such as Request and Response
using HttpServer    # Basic http/websockets server
using Mustache      # For populating html templates with data


# Define request handling
function mainhandler(req, res)
    if req.resource == "/home"
        tpl      = open(readall, "templates/home.html")                 # Retrieve template
        tpl_data = {"homepage_text" => "Hi, this is the home page."}    # Set data
        res.data = Mustache.render(tpl, tpl_data)                       # Return the template populated with data
    else
        res.status = 404
        res.data   = "Requested resource not found"
    end
    res
end


# Instantiate and run server
server = Server(mainhandler)    # Create Server instance with server.http.handle = mainhandler
run(server, 8000)               # This is a blocking function
