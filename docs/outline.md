# Outline

## Introduction

This page is an outline of documentation for this repository that will hopefully be written one day :)  It also indicates features that are yet to be implemented.

This repository is intended to:

- Enable people who aren't web developers to learn basic web development in Julia.
- Provide a set of example web applications written in Julia that can be adapted to various needs.  Most of the examples are geared towards data scientists who have never built a web application before, but they can be adapted to other applications.

Please note:

- I am learning this stuff as I go. Suggestions from more experienced web developers are welcome, as well as implementations of some missing features.
- There are lots of other features that could be added...feel free to contribute features, bug reports and anything else that may help.
- The examples deliberately avoid using frameworks such as ``Morsel.jl`` only because I have found it instructive to delve into the underlying libraries.

## Quick Start
See [here](crash_course.md) for a one-page introduction to web development.

## Examples
- [Example 1 - Hello World](https://bitbucket.org/jocklawrie/skeleton-webapp.jl/overview)
- [Example 2 - Templates](https://bitbucket.org/jocklawrie/skeleton-webapp.jl/overview)
- [Example 3 - Serving files](https://bitbucket.org/jocklawrie/skeleton-webapp.jl/overview)
- [Example 4 - Adding a middleware stack using ``Meddle.jl``](https://bitbucket.org/jocklawrie/skeleton-webapp.jl/overview)


# Under the Hood

## Make it work

- Example 1 - Hello World
	- The basic request/response pattern
                - Deep dive: Describe in detail the code in HttpCommon.jl and HttpServer.jl.
	- Returns some text to the client
- Example 2 - Templates
	- Writing data to a template, then serving the result
	- Returns text as part of a template
	- The returned text is hard coded on the server side...this example can be adapted to serve text and/or data that depends on user input.
- Example 3 - Serving files
	- Serves entire files
	- A data science example that serves data in templates as per Example 2, as well as static files for formatting (css files) and interactivity (JavaScript files). The result is an application that:
		- Reads data from a csv
		- Runs a linear regression
		- Produces some interactive charts that can be viewed in your browser
	- This example also demonstrates:
		- Running numerical computations in Julia as part of preparing the response
		- The use of GeoJSON data in mapping. See [Working With Mapping Data](working_with_mapping_data.md) for further details.
		- A formulaic approach to structuring web pages using the [Bootstrap](http://getbootstrap.com/javascript) framework developed at Twitter. See [Structuring Web Pages](structuring_web_pages.md) for further details.
- Example 4 - Adding a middleware stack using ``Meddle.jl``
	- This example adds a middleware stack to Example 3.
	- A middleware stack is a sequence of functions that operate on each request/response pair. ``Meddle.handle`` loops through each function in the stack. At each iteration, the function either:
		- Completes the request (response.finished == true), breaks the loop and returns the response, OR
		- Modifies the response and passes (req, res) to the next function in the stack, OR
		- Makes no modifications to the response and passes (req, res) to the next function in the stack
- Example 5 - Sessions
	- Add sessions to enable the server to "remember" the client. For example, this is helpful when the client must log in to access content. Without sessions the client would have to log in for every request.
	- Sessions will be implemented as client-side session to simplify scalability

## Make it secure

- Example 6 - Simple authentication (login)
	- Allow clients to access resources only if they are permitted
- Example 7 - Encryption
	- Use https to encrypt the conversation between the client and server so that eavesdroppers cannot listen in

## Make it fast

- Minify / Compile
	- Minimise the volume of content being returned.
	- See [Working With Mapping Data](working_with_mapping_data.md) for tips for reducing the size of GeoJSON data.
- Ajax: When a user requests updated data, serve only the updated data...don't reload the whole page.
- Content delivery networks: Many 3rd party libraries, which consist of static files, can be served from other servers that are part of *content delivery networks* (CDNs). This reduces the load on your server and reduces latency (the period between making a request and receiving the response). CDNs have many other uses as well.

## Miscellaneous

- Testing
- Deployment
- Using frameworks
