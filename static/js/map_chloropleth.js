var map;

function initmap(lat, lon, zm) {
	// set up the map
	map = new L.Map('map');

	// create the tile layer with correct attribution
	var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data &copy <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 4, maxZoom: 15, attribution: osmAttrib});		

	// start the map
	map.setView(new L.LatLng(lat, lon), zm);
	map.addLayer(osm);
}


function getColor(d) {
    // 8-colour scale
    return d > 0.875 ? '#800026' :
           d > 0.75  ? '#BD0026' :
           d > 0.625 ? '#E31A1C' :
           d > 0.5   ? '#FC4E2A' :
           d > 0.375 ? '#FD8D3C' :
           d > 0.25  ? '#FEB24C' :
           d > 0.125 ? '#FED976' :
                       '#FFEDA0';
}


function chloromapstyle(feature) {
    var sa2_name = feature.properties.SA2_NAME11;
    var y = response[sa2_name];
    return {
        fillColor: getColor(y),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.5
    };
}

///////////////////////////////////////////////
/// Listeners
///////////////////////////////////////////////

var geojson;

function highlightFeature(e) {
    // For mouseover
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }
    info.update(layer.feature.properties);
}


function resetHighlight(e) {
    // For mouseout
    geojson.resetStyle(e.target);
    info.update();
}


function zoomToFeature(e) {
    // onclick
    map.fitBounds(e.target.getBounds());
}


//// Add listeners
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}


///////////////////////////////////////////////
/// Information Box
///////////////////////////////////////////////

var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    var sa2_name = props ? props.SA2_NAME11 : "";
    var y = 1.0;    // Arbitrary value, changed if y is to be used
    if(sa2_name != "") {
        y = 0.01 * Math.round(100.0 * response[sa2_name]);
    }
    this._div.innerHTML = '<h4>Some Rate</h4>' +  (props ?
        '<b>' + props.SA2_NAME11 + '</b><br />' + y + ' cases / 100,000 people'
        : 'Hover over an area');
};


///////////////////////////////////////////////
/// Legend
///////////////////////////////////////////////

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875],
        labels = [];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(grades[i] + 0.1) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};
