# Why

This repository is intended to:

- Enable people who aren't web developers to learn basic web development in Julia.
- Provide a set of example web applications written in Julia that can be adapted to various needs.  Most of the examples are geared towards data scientists who have never built a web application before, but they can be adapted to other applications.

At the moment the [documentation](docs/outline.md) is thin but enough to get by. There are several examples in the _apps_ folder. These are described in the documentation [outline](docs/outline.md). The first is a simple _hello world_ example and each subsequent example adds features. For example, Example 3:

1. Reads data from a csv
2. Runs a linear regression
3. Produces some interactive charts that can be viewed in your browser

This basic pattern can be used, for example, to:

1. Read data from several sources (databases, websites, etc)
2. Spawn a new Julia process on [Google Compute Engine](http://www.blog.juliaferraioli.com/2013/12/julia-on-google-compute-engine.html) 
and run a simulation of the spread of an infectious disease
3. Display an animated map of the projected disease incidence over time


## Dependencies

For required Julia packages check the ``main_xxx.jl`` files for each example.
Dependencies that aren't Julia package are included in this repository.


## Usage

1. Clone this repo
2. `cd path/to/repo/apps/example_xxx`
3. `julia main_xxx.jl`
4. In your browser go to `localhost:8000/home`

## Todo:
1. Suggestions for making the application more flexible/robust/scalable/secure 
are always welcome. Feel free to contribute!

2. The documentation merely reflects my understanding, some of which I have 
inferred, perhaps incorrectly. Please feel free correct it where appropriate by filing an issue.

3. The charting library is [c3.js](http://c3js.org/), which provides many standard chart types that can 
be configured using JSON (i.e., server side using Julia code). See [here](http://c3js.org/examples.html)
for several common examples. I will add new chart types as my work demands it; 
for example, tables, maps, box plots, dashboards, etc. However, it is easy to add 
whatever templates, styling, javascript libraries and chart types you please.

4. The html templates are hard coded. For example, the 
application has a template with 2 tabs but not 3. To produce a template with 3 tabs 
simply modify the 2-tab template.
